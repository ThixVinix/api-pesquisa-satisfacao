const express = require("express");
const path = require("path");
const fs = require("fs");
const cors = require("cors");
const { features } = require("process");
const jwt = require("jsonwebtoken");
const SECRET = "SohEuSei";

const app = express();

//captura a requisição e se for uma req do tipo 
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(cors());

const porta = 3001

app.listen(porta, function () {
    console.log(`Servidor rodando na porta ${porta}`);
})

function verificaUser(req, resp, next) {
    const token = req.header("x-access-token")
    jwt.verify(token, SECRET, function (err, decoded) {
        if (err) {
            resp.json({ mensagem: "É necessário estar logado no sistema para realizar esta ação.", status: 401 }).end();
            //return resp.status(401).end()
        }
        //req.dec = decoded.user
        next()
    })
}

app.get("/login", function (req, resp) {

    resp.sendFile(__dirname + "/view/login.html");

});

app.post("/login", async function (req, resp) {

    let usuarioDigitado = String(req.body.user);
    let senhaDigitada = String(req.body.pass);

    console.log("Usuário digitado: " + usuarioDigitado);
    console.log("Senha digitada: " + senhaDigitada);

    let vetorUsuarios = [];
    vetorUsuarios = await readFileUsuarios();

    for (let i = 0; i < vetorUsuarios.length; i++) {
        const usuarioCadastrado = String(vetorUsuarios[i][0]);
        const senhaCadastrada = String(vetorUsuarios[i][1]);

        if ((usuarioDigitado === usuarioCadastrado) && (senhaDigitada === senhaCadastrada)) {
            console.log("ACESSO LIBERADO");
            //gerar token 
            const token = jwt.sign({ user: usuarioDigitado }, SECRET, { expiresIn: 60 })
            return resp.json({ auth: true, token })
        }

    }

    console.log("ACESSO NEGADO");
    //resp.status(401).end()
    resp.json({ auth: false, status: 401 }).end();

});

app.get("/recuperarSenha", function (req, resp) {

    resp.sendFile(__dirname + "/view/recuperar-senha.html");

});



app.get("/usuario", verificaUser, function (req, resp) {

    resp.json({
        msn: "Chegou na rota usuario - GET",
        user: req.query.nmUser,
        codigoUser: req.query.nmCodUser
    })
});

//localhost:3001/
app.get("/", async function (req, resp) {

    resp.sendFile(__dirname + "/view/index.html");

});

//localhost:3001/relatorio
app.get("/relatorio", async function (req, resp) {

    let matrizPesquisa = [];
    matrizPesquisa = await readFilePesquisa();

    let objEstatistica = inicializarObjetoJavascriptEstatistica();

    objEstatistica = calcularEstatistica(matrizPesquisa, objEstatistica);

    console.log("\nJSON GERADO COM A ESTATÍSTICA CALCULADA:");
    console.log(objEstatistica);


    resp.end(
        `
            <html>
                <head>
                    <meta charset="utf-8">
                    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
                </head>
                <body class="container" style="background: rgb(155,237,203);background: linear-gradient(103deg, rgba(155,237,203,1) 0%, rgba(199,249,244,1) 52%, rgba(113,186,213,1) 100%);">
                    <h1 class="display-6"> Percentual atendimentos: </h1>
                    <ul class="list-group shadow-lg">
                        <li class="list-group-item list-group-item-danger">Não atendida = ${objEstatistica.estatisticas.atendimentos.naoAtendida}%</li>
                        <li class="list-group-item list-group-item-warning">Parcialmente atendida = ${objEstatistica.estatisticas.atendimentos.parcialmenteAtendida}%</li>
                        <li class="list-group-item list-group-item-success">Totalmente atendida = ${objEstatistica.estatisticas.atendimentos.totalmenteAtendida}%</li>
                    </ul>
                    <h1 class="display-6"> Percentual comportamentos do funcionário: </h1>
                    <ul class="list-group shadow-lg">
                        <li class="list-group-item list-group-item-danger">Indelicado = ${objEstatistica.estatisticas.comportamentos.indelicado}%</li>
                        <li class="list-group-item list-group-item-warning">Mau humorado = ${objEstatistica.estatisticas.comportamentos.mauHumorado}%</li>
                        <li class="list-group-item list-group-item-primary">Neutro = ${objEstatistica.estatisticas.comportamentos.neutro}%</li>
                        <li class="list-group-item list-group-item-info">Educado = ${objEstatistica.estatisticas.comportamentos.educado}%</li>
                        <li class="list-group-item list-group-item-success">Muito Atencioso = ${objEstatistica.estatisticas.comportamentos.muitoAtencioso}%</li>
                    </ul>
                    <h1 class="display-6"> Percentual das notas do atendimento: </h1>
                    <ul class="list-group shadow-lg">
                        <li class="list-group-item list-group-item-danger">Nota 0 = ${objEstatistica.estatisticas.notas.nota0}%</li>
                        <li class="list-group-item list-group-item-secondary">Nota 1 = ${objEstatistica.estatisticas.notas.nota1}%</li>
                        <li class="list-group-item list-group-item-secondary">Nota 2 = ${objEstatistica.estatisticas.notas.nota2}%</li>
                        <li class="list-group-item list-group-item-secondary">Nota 3 = ${objEstatistica.estatisticas.notas.nota3}%</li>
                        <li class="list-group-item list-group-item-secondary">Nota 4 = ${objEstatistica.estatisticas.notas.nota4}%</li>
                        <li class="list-group-item list-group-item-warning">Nota 5 = ${objEstatistica.estatisticas.notas.nota5}%</li>
                        <li class="list-group-item list-group-item-secondary">Nota 6 = ${objEstatistica.estatisticas.notas.nota6}%</li>
                        <li class="list-group-item list-group-item-secondary">Nota 7 = ${objEstatistica.estatisticas.notas.nota7}%</li>
                        <li class="list-group-item list-group-item-secondary">Nota 8 = ${objEstatistica.estatisticas.notas.nota8}%</li>
                        <li class="list-group-item list-group-item-secondary">Nota 9 = ${objEstatistica.estatisticas.notas.nota9}%</li>
                        <li class="list-group-item list-group-item-success">Nota 10 = ${objEstatistica.estatisticas.notas.nota10}%</li>
                    </ul>
                    <h1 class="display-6"> Percentual da média de idade dos entrevistados: </h1>
                    <ul class="list-group shadow-lg">
                        <li class="list-group-item list-group-item-info">Abaixo de 15 anos = ${objEstatistica.estatisticas.idades.menosDe15}%</li>
                        <li class="list-group-item list-group-item-info">Entre 15 e 21 anos = ${objEstatistica.estatisticas.idades.entre15e21}%</li>
                        <li class="list-group-item list-group-item-info">Entre 22 e 35 anos = ${objEstatistica.estatisticas.idades.entre22e35}%</li>
                        <li class="list-group-item list-group-item-info">Entre 36 e 50 anos = ${objEstatistica.estatisticas.idades.entre36e50}%</li>
                        <li class="list-group-item list-group-item-info">Acima de 50 anos = ${objEstatistica.estatisticas.idades.acimaDe50}%</li>
                    </ul>
                    <h1 class="display-6"> Percentual dos gêneros dos entrevistados: </h1>
                    <ul class="list-group shadow-lg">
                        <li class="list-group-item list-group-item-info">Masculino = ${objEstatistica.estatisticas.generos.masculino}%</li>
                        <li class="list-group-item list-group-item-info">Feminino = ${objEstatistica.estatisticas.generos.feminino}%</li>
                        <li class="list-group-item list-group-item-info">LGBT = ${objEstatistica.estatisticas.generos.lgbt}%</li>
                    </ul>
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
                    crossorigin="anonymous"></script>
                </body>
            </html>
        `
    );
});


// Captura uma nova pesquisa de atendimento e grava no arquivo "pesquisa.csv" (http://localhost:3001/pesquisa).
app.post("/pesquisa", verificaUser, async function (req, resp) {

    let nome = req.body.nome;
    let atendimento = req.body.atendimento;
    let comportamentos = req.body.comportamentos;
    let nota = Number(req.body.nota);
    let idade = req.body.idade;
    let genero = req.body.genero;
    let timestamp = new Date().getTime();

    let textComp = String(comportamentos);

    while (textComp.includes(",")) {
        textComp = textComp.replace(",", ";");
    }

    let dados = `${nome},${atendimento},${textComp},${nota},${idade},${genero},${timestamp}\n`;

    let resposta = await salvarDadosPesquisa(dados);

    resp.send(resposta);

});

app.get("/estatisticas", async function (req, resp) {
    let matrizPesquisa = [];
    matrizPesquisa = await readFilePesquisa();

    let objEstatistica = inicializarObjetoJavascriptEstatistica();

    objEstatistica = calcularEstatistica(matrizPesquisa, objEstatistica);

    console.log("\nJSON GERADO COM A ESTATÍSTICA CALCULADA:");
    console.log(objEstatistica);

    resp.send(objEstatistica);
})

/**
 * @description Salva os dados da pesquisa no arquivo "pesquisa.csv".
 * @returns Retorna uma _promisse_ contendo a resposta em formato JSON (conseguindo salvar ou não).
 */
async function salvarDadosPesquisa(dados) {
    return new Promise((resolve, reject) => {
        fs.appendFile('pesquisa.csv', dados, function (err, data) {
            if (err) {
                console.log("Erro ao gravar os dados da pesquisa: " + err);
                reject(
                    {
                        "status": 500,
                        "mensagem": err
                    }
                );
            } else {
                console.log("Pesquisa Registrada Com sucesso!")
                resolve(
                    {
                        "status": 200,
                        "mensagem": "Pesquisa Registrada Com sucesso!"
                    });
            }
        });
    });
}

/**
 * @description Faz a leitura do arquivo "pesquisa.csv" par obter os dados da pesquisa.
 * @returns Retorna uma _promisse_ contendo uma matriz prenchida com as informações da pesquisa registrada no sistema.
 */
async function readFilePesquisa() {
    return new Promise((resolve, reject) => {
        fs.readFile('pesquisa.csv', "utf8", function (err, data) {
            if (err) {
                console.log("Erro ao ler arquivo: " + err);
                resolve(err);
            } else {
                let dadosPesquisa = []

                let infPesquisa = data.split("\n")

                for (let i = 0; i < infPesquisa.length; i++) {
                    const element = infPesquisa[i];
                    if (String(element) == "") {
                        break;
                    }
                    dadosPesquisa.push(element.split(","))
                }

                //Separa especialmente os comportamentos em um vetor específico dentro do vetor dos dados da pesquisa.
                for (let j = 0; j < dadosPesquisa.length; j++) {
                    for (let k = 0; k < dadosPesquisa[j][2].length; k++) {
                        let element = dadosPesquisa[j][2];
                        let textComportamentos = String(element).replace(";", ",");
                        dadosPesquisa[j][2] = textComportamentos.split(",");
                    }
                }
                console.log(dadosPesquisa);
                resolve(dadosPesquisa);
            }
        });
    });
}

/**
 * @description Faz a leitura do arquivo "usuarios.csv" par obter os dados da pesquisa.
 * @returns Retorna uma _promisse_ contendo uma matriz prenchida com as informações dos usuários registrados no sistema.
 */
async function readFileUsuarios() {
    return new Promise((resolve, reject) => {
        fs.readFile('usuarios.csv', "utf8", function (err, data) {
            if (err) {
                console.log("Erro ao ler arquivo: " + err);
                resolve(err);
            } else {
                let usuarios = [];

                let infUsuario = data.split("\r\n")

                for (let i = 0; i < infUsuario.length; i++) {
                    const element = infUsuario[i];
                    if (String(element) == "") {
                        break;
                    }
                    usuarios.push(element.split(","))
                }

                console.log(usuarios);
                resolve(usuarios);
            }
        });
    });
}

function inicializarObjetoJavascriptEstatistica() {
    let objEstatistica = {
        "estatisticas": {
            "atendimentos": {
                "naoAtendida": 0,
                "parcialmenteAtendida": 0,
                "totalmenteAtendida": 0
            },
            "comportamentos": {
                "indelicado": 0,
                "mauHumorado": 0,
                "neutro": 0,
                "educado": 0,
                "muitoAtencioso": 0
            },
            "notas": {
                "nota0": 0,
                "nota1": 0,
                "nota2": 0,
                "nota3": 0,
                "nota4": 0,
                "nota5": 0,
                "nota6": 0,
                "nota7": 0,
                "nota8": 0,
                "nota9": 0,
                "nota10": 0
            },
            "idades": {
                "menosDe15": 0,
                "entre15e21": 0,
                "entre22e35": 0,
                "entre36e50": 0,
                "acimaDe50": 0
            },
            "generos": {
                "masculino": 0,
                "feminino": 0,
                "lgbt": 0
            }
        }
    };

    return objEstatistica;
}

function calcularEstatistica(matrizPesquisa, obj) {

    let tamanhoPesquisa = matrizPesquisa.length;
    let qtdTotalVariaveisComportamento = 0;

    for (let i = 0; i < tamanhoPesquisa; i++) {
        obj.estatisticas.atendimentos = contabilizarAtendimento(obj.estatisticas.atendimentos, matrizPesquisa[i][1]);

        for (let k = 0; k < matrizPesquisa[i][2].length; k++) {
            const comportamento = matrizPesquisa[i][2][k];
            obj.estatisticas.comportamentos = contabilizarComportamento(obj.estatisticas.comportamentos, comportamento);
            qtdTotalVariaveisComportamento++;
        }

        obj.estatisticas.notas = contabilizarNota(obj.estatisticas.notas, matrizPesquisa[i][3]);

        obj.estatisticas.idades = contabilizarMediaIdades(obj.estatisticas.idades, matrizPesquisa[i][4]);

        obj.estatisticas.generos = contabilizarGenero(obj.estatisticas.generos, matrizPesquisa[i][5]);
    }

    obj.estatisticas.atendimentos = calcularPercentuaisAtendimento(obj.estatisticas.atendimentos, tamanhoPesquisa);
    obj.estatisticas.comportamentos = calcularPercentuaisComportamento(obj.estatisticas.comportamentos, qtdTotalVariaveisComportamento);
    obj.estatisticas.notas = calcularPercentuaisNota(obj.estatisticas.notas, tamanhoPesquisa);
    obj.estatisticas.idades = calcularPercentuaisIdade(obj.estatisticas.idades, tamanhoPesquisa);
    obj.estatisticas.generos = calcularPercentuaisGenero(obj.estatisticas.generos, tamanhoPesquisa);

    console.log("\nEstatística atendimentos: \n" + obj.estatisticas.atendimentos.naoAtendida + " " + obj.estatisticas.atendimentos.parcialmenteAtendida + " " + obj.estatisticas.atendimentos.totalmenteAtendida);
    console.log("Estatística comportamentos: \n" + obj.estatisticas.comportamentos.indelicado + " " + obj.estatisticas.comportamentos.mauHumorado + " " + obj.estatisticas.comportamentos.neutro + " " + obj.estatisticas.comportamentos.educado + " " + obj.estatisticas.comportamentos.muitoAtencioso);
    console.log("Estatística notas: \n" + obj.estatisticas.notas.nota0 + " " + obj.estatisticas.notas.nota1 + " " + obj.estatisticas.notas.nota2 + " " + obj.estatisticas.notas.nota3 + " " + obj.estatisticas.notas.nota4 + " " + obj.estatisticas.notas.nota5 + " " + obj.estatisticas.notas.nota6 + " " + obj.estatisticas.notas.nota7 + " " + obj.estatisticas.notas.nota8 + " " + obj.estatisticas.notas.nota9 + " " + obj.estatisticas.notas.nota10 + " ");
    console.log("Estatística média idade: \n" + obj.estatisticas.idades.menosDe15 + " " + obj.estatisticas.idades.entre15e21 + " " + obj.estatisticas.idades.entre22e35 + " " + obj.estatisticas.idades.entre36e50 + " " + obj.estatisticas.idades.acimaDe50);
    console.log("Estatística gêneros: \n" + obj.estatisticas.generos.masculino + " " + obj.estatisticas.generos.feminino + " " + obj.estatisticas.generos.lgbt);

    return obj;
}

function contabilizarAtendimento(estatisticaAtendimento, atendimento) {

    switch (atendimento) {
        case "Não foi atendida":
            estatisticaAtendimento.naoAtendida++;
            break;
        case "Parcialmente atendida":
            estatisticaAtendimento.parcialmenteAtendida++;
            break;
        case "Totalmente atendida":
            estatisticaAtendimento.totalmenteAtendida++;
            break;
        default:
            break;
    }

    return estatisticaAtendimento;
}

function contabilizarComportamento(estatisticaComportamento, comportamento) {

    switch (comportamento) {
        case "Indelicado":
            estatisticaComportamento.indelicado++;
            break;
        case "Mau humorado":
            estatisticaComportamento.mauHumorado++;
            break;
        case "Neutro":
            estatisticaComportamento.neutro++;
            break;
        case "Educado":
            estatisticaComportamento.educado++;
            break;
        case "Muito Atencioso":
            estatisticaComportamento.muitoAtencioso++;
            break;
        default:
            break;
    }

    return estatisticaComportamento;
}

function contabilizarNota(estatisticaNota, nota) {

    switch (Number(nota)) {
        case 0:
            estatisticaNota.nota0++;
            break;
        case 1:
            estatisticaNota.nota1++;
            break;
        case 2:
            estatisticaNota.nota2++;
            break;
        case 3:
            estatisticaNota.nota3++;
            break;
        case 4:
            estatisticaNota.nota4++;
            break;
        case 5:
            estatisticaNota.nota5++;
            break;
        case 6:
            estatisticaNota.nota6++;
            break;
        case 7:
            estatisticaNota.nota7++;
            break;
        case 8:
            estatisticaNota.nota8++;
            break;
        case 9:
            estatisticaNota.nota9++;
            break;
        case 10:
            estatisticaNota.nota10++;
            break;
        default:
            break;
    }

    return estatisticaNota;
}

function contabilizarMediaIdades(estatisticaIdade, idade) {

    if (idade < 15) {
        estatisticaIdade.menosDe15++;
    } else if (idade >= 15 && idade <= 21) {
        estatisticaIdade.entre15e21++;
    } else if (idade >= 22 && idade <= 35) {
        estatisticaIdade.entre22e35++;
    } else if (idade >= 36 && idade <= 50) {
        estatisticaIdade.entre36e50++;
    } else {
        estatisticaIdade.acimaDe50++;
    }

    return estatisticaIdade;
}

function contabilizarGenero(estatisticaGenero, genero) {

    switch (genero) {
        case "Masculino":
            estatisticaGenero.masculino++;
            break;
        case "Feminino":
            estatisticaGenero.feminino++;
            break;
        case "LGBT":
            estatisticaGenero.lgbt++;
            break;
        default:
            break;
    }

    return estatisticaGenero;

}

function calcularPercentuaisAtendimento(estatisticaAtendimento, qtdTotalVariaveis) {
    estatisticaAtendimento.naoAtendida = calcularPercentual(estatisticaAtendimento.naoAtendida, qtdTotalVariaveis);
    estatisticaAtendimento.parcialmenteAtendida = calcularPercentual(estatisticaAtendimento.parcialmenteAtendida, qtdTotalVariaveis);
    estatisticaAtendimento.totalmenteAtendida = calcularPercentual(estatisticaAtendimento.totalmenteAtendida, qtdTotalVariaveis);
    return estatisticaAtendimento;
}

function calcularPercentuaisComportamento(estatisticaComportamento, qtdTotalVariaveis) {
    estatisticaComportamento.indelicado = calcularPercentual(estatisticaComportamento.indelicado, qtdTotalVariaveis);
    estatisticaComportamento.mauHumorado = calcularPercentual(estatisticaComportamento.mauHumorado, qtdTotalVariaveis);
    estatisticaComportamento.neutro = calcularPercentual(estatisticaComportamento.neutro, qtdTotalVariaveis);
    estatisticaComportamento.educado = calcularPercentual(estatisticaComportamento.educado, qtdTotalVariaveis);
    estatisticaComportamento.muitoAtencioso = calcularPercentual(estatisticaComportamento.muitoAtencioso, qtdTotalVariaveis);
    return estatisticaComportamento;
}

function calcularPercentuaisNota(estatisticaNota, qtdTotalVariaveis) {
    estatisticaNota.nota0 = calcularPercentual(estatisticaNota.nota0, qtdTotalVariaveis);
    estatisticaNota.nota1 = calcularPercentual(estatisticaNota.nota1, qtdTotalVariaveis);
    estatisticaNota.nota2 = calcularPercentual(estatisticaNota.nota2, qtdTotalVariaveis);
    estatisticaNota.nota3 = calcularPercentual(estatisticaNota.nota3, qtdTotalVariaveis);
    estatisticaNota.nota4 = calcularPercentual(estatisticaNota.nota4, qtdTotalVariaveis);
    estatisticaNota.nota5 = calcularPercentual(estatisticaNota.nota5, qtdTotalVariaveis);
    estatisticaNota.nota6 = calcularPercentual(estatisticaNota.nota6, qtdTotalVariaveis);
    estatisticaNota.nota7 = calcularPercentual(estatisticaNota.nota7, qtdTotalVariaveis);
    estatisticaNota.nota8 = calcularPercentual(estatisticaNota.nota8, qtdTotalVariaveis);
    estatisticaNota.nota9 = calcularPercentual(estatisticaNota.nota9, qtdTotalVariaveis);
    estatisticaNota.nota10 = calcularPercentual(estatisticaNota.nota10, qtdTotalVariaveis);
    return estatisticaNota;
}

function calcularPercentuaisIdade(estatisticaIdade, qtdTotalVariaveis) {
    estatisticaIdade.menosDe15 = calcularPercentual(estatisticaIdade.menosDe15, qtdTotalVariaveis);
    estatisticaIdade.entre15e21 = calcularPercentual(estatisticaIdade.entre15e21, qtdTotalVariaveis);
    estatisticaIdade.entre22e35 = calcularPercentual(estatisticaIdade.entre22e35, qtdTotalVariaveis);
    estatisticaIdade.entre36e50 = calcularPercentual(estatisticaIdade.entre36e50, qtdTotalVariaveis);
    estatisticaIdade.acimaDe50 = calcularPercentual(estatisticaIdade.acimaDe50, qtdTotalVariaveis);
    return estatisticaIdade;
}

function calcularPercentuaisGenero(estatisticaGenero, qtdTotalVariaveis) {
    estatisticaGenero.masculino = calcularPercentual(estatisticaGenero.masculino, qtdTotalVariaveis);
    estatisticaGenero.feminino = calcularPercentual(estatisticaGenero.feminino, qtdTotalVariaveis);
    estatisticaGenero.lgbt = calcularPercentual(estatisticaGenero.lgbt, qtdTotalVariaveis);
    return estatisticaGenero;
}

function calcularPercentual(variavel, qtdTotalVariaveis) {
    return ((variavel / qtdTotalVariaveis) * 100).toFixed(2);
}