/* Valores do atendimento do cliente */
var qtdNaoAtendida = 0;
var qtdParcialmenteAtendida = 0;
var qtdTotalmenteAtendida = 0;

/* Valores do comportamento do atendente */
var qtdIndelicado = 0;
var qtdMauHumorado = 0;
var qtdNeutro = 0;
var qtdDelicado = 0;
var qtdMuitoAtencioso = 0;


function capturarSelecaoAvaliacao(params) {

}


function capturarSelecaoComportamentoAtendente() {
    var opcoesComp = document.querySelectorAll(".comp");
    for (let index = 0; index < opcoesComp.length; index++) {
        if (opcoesComp[index].checked) {
            switch (opcoesComp[index].value) {
                case "opt1":
                    qtdIndelicado++;
                    break;
                case "opt2":
                    qtdMauHumorado++;
                    break;
                case "opt3":
                    qtdNeutro++;
                    break;
                case "opt4":
                    qtdDelicado++;
                    break;
                case "opt5":
                    qtdMuitoAtencioso++;
                    break;
                default:
                    break;
            }
        }
    }

    exibirLogResultadoComportamentoAtendente();
}

function exibirLogResultadoComportamentoAtendente() {

    const descInicial = "Qtd. de marcações ";

    console.log(
        "[SITUAÇÃO DO COMPORTAMENTO DO ATENDENTE]" + "\n" +
        descInicial + "'Indelicado':\t\t\t" + qtdIndelicado + "\n" +
        descInicial + "'Mau humorado':\t\t" + qtdMauHumorado + "\n" +
        descInicial + "'Neutro':\t\t\t\t" + qtdNeutro + "\n" +
        descInicial + "'Delicado':\t\t\t" + qtdDelicado + "\n" +
        descInicial + "'Muito atencioso':\t" + qtdMuitoAtencioso + "\n"
    )
}