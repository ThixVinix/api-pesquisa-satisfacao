var divAlert = document.querySelector("#idDivAlert");
var opcoesComportamento = document.querySelectorAll(".comp");
var btEnviar = document.querySelector("#idBtEnviar");
var btResetar = document.querySelector("#idBtResetar");
var btVisualizarRelatorio = document.querySelector("#idBtVisualizarRelatorio");

// AO CLICAR NO BOTÃO "ENVIAR".
btEnviar.addEventListener("click", async (event) => {
    event.preventDefault();
    desabilitarTodosBotoes();
    removerComponentesInternos(divAlert);
    let carregamento = exibirCarregamento("Salvando os dados");
    exibirMensagem(carregamento, "alert-info", false);

    let form = document.querySelector("#idFormPesquisa");

    let isValid = validarCampos(form);

    if (!isValid) {
        habilitarTodosBotoes();
        return;
    }

    let comportamentos = [];

    opcoesComportamento.forEach(element => {
        if (element.checked) {
            comportamentos.push(element.value);
        }
    });

    const dados = {
        nome: form.nmNome.value,
        atendimento: form.nmAtendimento.value,
        comportamentos: comportamentos,
        nota: form.nmRange.value,
        idade: form.nmIdade.value,
        genero: form.nmGenero.value,
    }

    console.log(dados);
    
    localStorage.getItem(token);

    const options = {
        method: "POST",
        headers: { "Content-Type": "application/json", "x-access-token": token },
        body: JSON.stringify(dados),
    }

    let response = await enviarDados(options);

    if (response == -1) {
        return;
    }

    console.log(response);

    exibirRespostaServidor(response, form);

});

function validarCampos(form) {

    if (form.nmNome.value.trim() == "") {
        removerComponentesInternos(divAlert);
        exibirMensagem("Favor, preencha o campo \"Nome\"", "alert-warning", true);
        form.nmNome.focus();
        window.scrollTo(0, 0);
        return false;
    }

    if (form.nmAtendimento.value.trim() == "") {
        removerComponentesInternos(divAlert);
        exibirMensagem("Favor, selecione uma opção do atendimento", "alert-warning", true);
        window.scrollTo(0, 0);
        return false;
    }

    let checado = false;
    for (let index = 0; index < opcoesComportamento.length; index++) {
        if (opcoesComportamento[index].checked) {
            checado = true;
            break;
        }
    }

    if (!checado) {
        removerComponentesInternos(divAlert);
        exibirMensagem("Favor, selecione ao menos 1 opção do comportamento do atendente.", "alert-warning", true);
        window.scrollTo(0, 0);
        return false;
    }

    if (form.nmIdade.value.trim() == "") {
        removerComponentesInternos(divAlert);
        exibirMensagem("Favor, preencha o campo \"Idade\"", "alert-warning", true);
        form.nmIdade.focus();
        window.scrollTo(0, 0);
        return false;
    }

    if (form.nmGenero.value.trim() == "") {
        removerComponentesInternos(divAlert);
        exibirMensagem("Favor, selecione uma opção de gênero.", "alert-warning", true);
        window.scrollTo(0, 0);
        return false;
    }

    return true;
}

async function enviarDados(options) {
    let response;
    try {
        response = await fetch("http://localhost:3001/pesquisa", options);
    } catch (error) {
        habilitarTodosBotoes();
        console.log("Erro ao salvar os dados (verifique se o servidor está disponível) --- " + error);
        removerComponentesInternos(divAlert);
        exibirMensagem("<b>Não foi possível salvar a pesquisa.</b><br>Consulte o administrador do sistema.", "alert-danger", true);
        window.scrollTo(0, 0);
        return -1;
    }

    return response.json();
}

function habilitarTodosBotoes() {
    btEnviar.disabled = false;
    btResetar.disabled = false;
    btVisualizarRelatorio.disabled = false;
}

function desabilitarTodosBotoes() {
    btEnviar.disabled = true;
    btResetar.disabled = true;
    btVisualizarRelatorio.disabled = true;
}

function exibirRespostaServidor(response, form) {
    switch (response.status) {
        case 200:
            form.reset();
            removerComponentesInternos(divAlert);
            exibirMensagem(response.mensagem, "alert-success", false);
            window.scrollTo(0, 0);
            bloquearBotoesPor2Segundos();
            break;

        case 401:
            removerComponentesInternos(divAlert);
            exibirMensagem(response.mensagem, "alert-danger", false);
            window.scrollTo(0, 0);
        break;
        
        case 500:
            removerComponentesInternos(divAlert);
            exibirMensagem(response.mensagem, "alert-danger", true);
            window.scrollTo(0, 0);
            habilitarTodosBotoes();
            break;

        default:
            removerComponentesInternos(divAlert);
            exibirMensagem(response.mensagem, "alert-danger", true);
            window.scrollTo(0, 0);
            habilitarTodosBotoes();
            break;
    }
}

function bloquearBotoesPor2Segundos() {
    setTimeout(function () {
        removerComponentesInternos(divAlert);
        habilitarTodosBotoes();
    }, 2000);
}

/* async function getFotosPadrao() {
    let response;
    try {
        response = await fetch("http://localhost:3001/fotospadrao");
        console.log(response);
    } catch (error) {
        removerComponentesInternos(divAlert);
        exibirMensagem("<b>Erro ao carregar as fotos padrões.</b><br>Consulte o administrador do sistema", "alert-danger", true);
        console.log("Erro ao carregar as fotos padrão (verifique se o servidor está disponível) --- " + error);
        return [];
    }

    return response.json();
} */

function removerComponentesInternos(father) {
    while (father.firstChild) {
        father.removeChild(father.firstChild);
    }
}

function limparCampos() {
    inputRg.value = "";
    inputVoto.value = "";
    imagemCand.src =
        alterarCorCandidatoNaoIdentificado();
}

function exibirMensagem(mensagem, corCaixa, contemBotao) {
    let alert = document.createElement("div")
    alert.className = "alert alert-dismissible fade show shadow-sm"
    alert.classList.add(corCaixa);
    alert.role = "alert";
    alert.innerHTML = `${mensagem}`
    divAlert.appendChild(alert);

    if (contemBotao) {
        var closeBt = document.createElement("button");
        closeBt.type = "button"
        closeBt.classList.add("btn-close")
        closeBt.setAttribute("data-bs-dismiss", "alert")
        closeBt.setAttribute("aria-label", "Close")
        alert.appendChild(closeBt);
    }
}

function exibirCarregamento(mensagem) {
    let conteudoCarregamento = `<div class="d-flex align-items-center">
                                    <strong>${mensagem}...</strong>
                                <div class="spinner-border ms-auto" role="status" aria-hidden="true"></div>
                                </div>`

    return conteudoCarregamento;
}