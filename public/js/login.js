var form = document.querySelector("#idFormLogin");
var divAlert = document.querySelector("#idDivAlert");
var btAcessar = document.querySelector("#idBtAcessar");


btAcessar.addEventListener("click", async function (event) {
    event.preventDefault();

    let usuario = form.nmUsuario.value;
    let senha = form.nmSenha.value;

    if (usuario == null || usuario.trim() == "") {
        removerComponentesInternos(divAlert);
        exibirMensagem("Preencha o campo \"Usuário\".", "alert-primary", true);
        return;
    }

    if (senha == null || senha.trim() == "") {
        removerComponentesInternos(divAlert);
        exibirMensagem("Preencha o campo \"Senha\".", "alert-primary", true);
        return;
    }


    let resposta = await acessarSistema(usuario, senha);

    removerComponentesInternos(divAlert);
    
    if (resposta.status == 401) {
        exibirMensagem("Usuário ou Senha inválido(s).", "alert-danger", true);
    }

    localStorage.removeItem("token");
    localStorage.setItem("token", resposta.token);

    console.log(resposta);
    var autenticador = localStorage.getItem("token");
    console.log(autenticador);

});


async function acessarSistema(login, senha) {
    var options = {
        method: "POST",
        body: JSON.stringify({
            user: login,
            pass: senha
        }),
        headers: { "Content-Type": "application/json" }
    }


    var resposta = await fetch("http://localhost:3001/login", options)

    return resposta.json();
}

function removerComponentesInternos(father) {
    while (father.firstChild) {
        father.removeChild(father.firstChild);
    }
}

function exibirMensagem(mensagem, corCaixa, contemBotao) {
    let alert = document.createElement("div")
    alert.className = "alert alert-dismissible fade show shadow-sm"
    alert.classList.add(corCaixa);
    alert.role = "alert";
    alert.innerHTML = `${mensagem}`
    divAlert.appendChild(alert);

    if (contemBotao) {
        var closeBt = document.createElement("button");
        closeBt.type = "button"
        closeBt.classList.add("btn-close")
        closeBt.setAttribute("data-bs-dismiss", "alert")
        closeBt.setAttribute("aria-label", "Close")
        alert.appendChild(closeBt);
    }
}